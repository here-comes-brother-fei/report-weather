#ifndef WIDGET_H
#define WIDGET_H

#include <QMenu>
#include <QWidget>
#include <qlabel.h>
#include <qnetworkreply.h>
#include "citycodeuitls.h"
#include"day.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Day days[7];
    QList<QLabel*> mDateList;
    QList<QLabel*> mWeekList;
    QList<QLabel*> mIconList;
    QList<QLabel*> mWeaTypeList;
    QList<QLabel*> mAirqList;
    QList<QLabel*> mFxList;
    QList<QLabel*> mFlList;

    Widget(QWidget *parent = nullptr);
    ~Widget();
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    bool eventFilter(QObject *watched, QEvent *event);

public slots:
    void readHttpRply(QNetworkReply *reply);

private slots:
    void on_pushButton_clicked();

    void on_lineEdit_returnPressed();

private:
    Ui::Widget *ui;
    QMenu *menuQuit;
    QPoint mOffset;
    QNetworkReply *reply;
    QString strUrl;
    QNetworkAccessManager *manager;
    CityCodeUitls cityCodeUtils;
    QMap<QString,QString> mTypeMap;
    void parseWeatherJsonData(QByteArray rawData);
    void parseWeatherJsonDataNew(QByteArray rawData);
    void updateUI();
    void drawTempLineHigh();
    void drawTempLineLow();
};
#endif // WIDGET_H
