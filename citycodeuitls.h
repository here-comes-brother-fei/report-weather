#ifndef CITYCODEUITLS_H
#define CITYCODEUITLS_H

#include <qmap.h>



class CityCodeUitls
{
public:
    CityCodeUitls();
    QMap<QString,QString> cityMap;
    QString getCityCodeFromName(QString cityName);
    void initCityMap();
};

#endif // CITYCODEUITLS_H
